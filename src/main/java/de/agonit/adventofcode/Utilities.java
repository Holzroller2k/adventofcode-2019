package de.agonit.adventofcode;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Utilities {

	public static Path getInputPath(int day) throws URISyntaxException {
		return Paths.get(ClassLoader.getSystemResource(String.format("input/%d.input", day)).toURI());
	}

	public static List<String> getInputList(int day) throws IOException, URISyntaxException {
		return Files.readAllLines(getInputPath(day), Charset.forName("UTF-8"));
	}
}
