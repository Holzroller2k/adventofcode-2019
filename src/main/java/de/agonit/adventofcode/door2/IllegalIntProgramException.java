package de.agonit.adventofcode.door2;


public class IllegalIntProgramException extends RuntimeException {

	public IllegalIntProgramException(String errorMessage) {
		super(errorMessage);
	}

}
