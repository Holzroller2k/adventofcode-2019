package de.agonit.adventofcode.door2;

import java.util.Arrays;
import java.util.Scanner;

import de.agonit.adventofcode.Utilities;

public class Door2 {

	private static final int EXPECTED_OUTPUT_CODE = 19690720;
	private static final int PROGRAM_OFFSETSIZE = 4;

	public static void main(String[] args) throws Exception {
		String inputLine;

		try (Scanner scanner = new Scanner(Utilities.getInputPath(2))) {
			inputLine = scanner.nextLine();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		int[] intProgram = Arrays.stream(inputLine.split(",")).mapToInt(Integer::parseInt).toArray();

		intProgram[1] = 12;
		intProgram[2] = 2;

		System.out.println("Result code: " + getIntComputerOutputCode(getCopy(intProgram)));
		
		int[] inputValuesForOutputCode = getInputValuesForOutputCode(intProgram, EXPECTED_OUTPUT_CODE);
		System.out.println(String.format("Input values for outputCode '%d': %s", EXPECTED_OUTPUT_CODE, Arrays.toString(inputValuesForOutputCode)));
		
		System.out.println("Answer is: " + (100 * inputValuesForOutputCode[0] +  inputValuesForOutputCode[1]));
	}

	public static int getIntComputerOutputCode(int[] intcodeProgramm) {
		int offset = 0;

		while (intcodeProgramm[offset] != 99 && runStep(intcodeProgramm, offset)) {
			offset += PROGRAM_OFFSETSIZE;
		}

		return intcodeProgramm[0];
	}

	private static boolean runStep(int[] intcodeProgramm, int offset) {

		try {
			int opcode = intcodeProgramm[offset];
			int operand1Position = intcodeProgramm[offset + 1];
			int operand1 = intcodeProgramm[operand1Position];
			int operand2Position = intcodeProgramm[offset + 2];
			int operand2 = intcodeProgramm[operand2Position];
			int targetPosition = intcodeProgramm[offset + 3];
			switch (opcode) {
				case 1:
					intcodeProgramm[targetPosition] = operand1 + operand2;
					return true;
				case 2:
					intcodeProgramm[targetPosition] = operand1 * operand2;
					return true;
				case 99:
					return false;
				default:
					throw new IllegalIntProgramException(String.format("Unknown opcode!: '%d'", opcode));
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalIntProgramException(String.format("Operand tries to access outside of array bounds!: '%s'", Arrays.toString(intcodeProgramm)));
		}

	}

	public static int[] getInputValuesForOutputCode(int[] intcodeProgramm, int expectedOutputCode) {
		int[] detectedInputValues = new int[2];

		for (int i2 = 0; i2 <= intcodeProgramm.length-1; i2++) {
			for (int i1 = 0; i1 <= intcodeProgramm.length-1; i1++) {
				detectedInputValues[0] = i1;
				detectedInputValues[1] = i2;

				int[] currentProgramAttempt = Arrays.copyOf(intcodeProgramm, intcodeProgramm.length);
				currentProgramAttempt[1] = i1;
				currentProgramAttempt[2] = i2;

				int currentOutputCode = -1;
				try {
					currentOutputCode = getIntComputerOutputCode(currentProgramAttempt);
				} catch (IllegalIntProgramException e) {
					// Do nothing, next attempt
				}

				if (expectedOutputCode == currentOutputCode) {
					return detectedInputValues;
				}
			}
		}

		throw new IllegalArgumentException(String.format("No input values between 0 and 99 for outputCode '%s'", expectedOutputCode));
	}

	public static int[] getCopy(int[] intProgramm) {
		return Arrays.copyOf(intProgramm, intProgramm.length);
	}
}
