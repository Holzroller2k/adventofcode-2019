package de.agonit.adventofcode.door3;


public class WireStep {

	Direction direction;
	int distance;
	
	public WireStep(Direction direction, int distance) {
		this.direction = direction;
		this.distance = distance;
	}

	public static WireStep from(String input) {
		Direction direction = Direction.valueOf(input.substring(0, 1));
		int distance = Integer.parseInt(input.substring(1));
		return new WireStep(direction, distance);
	}
	
	public Coordinate getVector() {
		Coordinate vector = direction.getVector();
		vector.x *= distance;
		vector.y *= distance;
		return vector;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + distance;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WireStep other = (WireStep) obj;
		if (direction != other.direction)
			return false;
		if (distance != other.distance)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%s%s",direction,distance);
	}
	
	
}
