package de.agonit.adventofcode.door3;


public class Intersection {
	CablePath pathA;
	CablePath pathB;
	Coordinate intersectionPoint;
	
	public Intersection(CablePath pathA, CablePath pathB, Coordinate intersectionPoint) {
		this.pathA = pathA;
		this.pathB = pathB;
		this.intersectionPoint = intersectionPoint;
	}

	@Override
	public String toString() {
		return String.format("%s X %s @ %s", pathA, pathB, intersectionPoint);
	}
	
}
