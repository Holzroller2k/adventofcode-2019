package de.agonit.adventofcode.door3;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PathManager {
	final static Logger LOG = LoggerFactory.getLogger(PathManager.class);

	Coordinate current = new Coordinate(0, 0);
	List<CablePath> cablePaths = new ArrayList<>();
	
	public List<CablePath> getCablePath(List<WireStep> steps) {

		for (WireStep step : steps) {
			LOG.trace("================");
			LOG.trace("Transforming WireStep {} to CablePath...", step);
			LOG.trace("Current from coordinate: {}", current);
			
			Coordinate to = current.mutate(step.getVector());
			LOG.trace("Target coordinate: {}", to);
			
			CablePath cablePath = new CablePath(current, to);
			LOG.trace("Adding resulting CablePath: {}", cablePath);
			cablePaths.add(cablePath);
			current = to;
		}
		
		return cablePaths;
	}
}
