package de.agonit.adventofcode.door3;

public enum Direction {
	U {
		@Override
		public Coordinate getVector() {
			return new Coordinate(0, 1);
		}

	},
	D {
		@Override
		public Coordinate getVector() {
			return new Coordinate(0, -1);
		}
	},
	L {
		@Override
		public Coordinate getVector() {
			return new Coordinate(-1, 0);
		}
	},
	R {
		@Override
		public Coordinate getVector() {
			return new Coordinate(1, 0);
		}
	};

	public abstract Coordinate getVector();
}
