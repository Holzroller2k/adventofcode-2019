package de.agonit.adventofcode.door3;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.agonit.adventofcode.Utilities;


public class Door3 {
	
	final static Logger LOG = LoggerFactory.getLogger(Door3.class);

	public static void main(String[] args) throws Exception {
		List<String> inputList = Utilities.getInputList(3);
		
		LOG.debug("Imported inputList with {} lines", inputList.size());
		
		List<CablePath> cable1 = new PathManager().getCablePath(getSteps(inputList.get(0)));
		List<CablePath> cable2 = new PathManager().getCablePath(getSteps(inputList.get(1)));
		
		Map<CablePath, List<Intersection>> intersections = getIntersections(cable1, cable2);
		
		Optional<Intersection> min = getClosestIntersection(intersections);
		
		Intersection closestIntersection = min.orElseThrow(() -> new IllegalArgumentException("No intersection found!"));
		
		System.out.println("Closest coordinate" + closestIntersection);
		
		System.out.println("Closest distance: " + closestIntersection.intersectionPoint.getDistanceToOutput());
	}

	public static Optional<Intersection> getClosestIntersection(Map<CablePath, List<Intersection>> intersections) {
		Optional<Intersection> min = intersections.values().parallelStream().flatMap(Collection::stream).min(Comparator.comparingInt(i -> i.intersectionPoint.getDistanceToOutput()));
		return min;
	}

	public static Map<CablePath, List<Intersection>> getIntersections(List<CablePath> cable1, List<CablePath> cable2) {
		Map<CablePath, List<Intersection>> intersections = new HashMap<>();
		cable1.parallelStream()
		.forEach(
				c1 -> {
					intersections.put(c1,
					cable2.parallelStream()
					.filter(c2 -> CablePath.getIntersection(c1, c2).isPresent())
					.map(c2 -> { return CablePath.getIntersection(c1, c2).get();})
					.collect(Collectors.toList()));
				}
		);
		return intersections;
	}
	
	public static List<WireStep> getSteps(String input) {
		String[] split = input.split(",");
		
		return Arrays.stream(split).map(String::trim).map(WireStep::from).collect(Collectors.toList());
	}
}
