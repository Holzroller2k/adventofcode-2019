package de.agonit.adventofcode.door3;

public class Coordinate {

	int x;
	int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Coordinate mutate(Coordinate vector) {
		return new Coordinate(x + vector.x, y + vector.y);
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	public int getDistanceToOutput() {
		return getManhattenDistance(this, new Coordinate(0, 0));
	}

	public static int getManhattenDistance(Coordinate a, Coordinate b) {
		return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
	}

	@Override
	public String toString() {
		return String.format("(%d,%d)", x, y);
	}
}
