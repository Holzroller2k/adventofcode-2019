package de.agonit.adventofcode.door3;

import java.util.Optional;

public class CablePath {

	final Coordinate from;
	final Coordinate to;

	public CablePath(Coordinate from, Coordinate to) {
		this.from = from;
		this.to = to;
	}

	public boolean isHorizontal() {
		return (from.y == to.y);
	}
	
	public Optional<Intersection> getIntersection(CablePath other) {
		Optional<Intersection> potentialIntersection = Optional.empty();
		if (this.isHorizontal() && !other.isHorizontal()) {
			//other is vertical, from and to have same x
			int othersX = other.from.x;
			if (between(from.x, othersX, to.x) && between(other.from.y, from.y, other.to.y)) {
				potentialIntersection = Optional.of(new Intersection(this, other, new Coordinate(othersX, from.y)));
			}
		} else if (!this.isHorizontal() && other.isHorizontal()) {
			//other is horizontal, from and to have same y
			int othersY = other.from.y;
			if (between(from.y, othersY, to.y) && between(other.from.x, from.x, other.to.x)) {
				potentialIntersection = Optional.of(new Intersection(this, other, new Coordinate(from.x, othersY)));
			}
		}
		
		return potentialIntersection;
	}
	
	public static Optional<Intersection> getIntersection(CablePath a, CablePath b) {
		return a.getIntersection(b);
	}
	
	public static boolean between(int border1, int testSubject, int border2) {
		int lowerBorder = Math.min(border1, border2);
		int upperBorder = Math.max(border1, border2);
		return (testSubject > lowerBorder && testSubject < upperBorder);
	}
	
	@Override
	public String toString() {
		return String.format("(%d,%d) -> (%d,%d)", from.x, from.y, to.x, to.y);
	}

}
