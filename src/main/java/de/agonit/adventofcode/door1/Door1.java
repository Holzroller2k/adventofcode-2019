package de.agonit.adventofcode.door1;

import java.util.List;

import de.agonit.adventofcode.Utilities;

public class Door1 {

	public static void main(String[] args) throws Exception {
		List<String> moduleMasses = Utilities.getInputList(1);
		
		final int fuelForModules = moduleMasses.parallelStream().mapToInt(Integer::parseInt).map(Door1::getFuelPerMass).reduce((a, b) -> a + b).orElse(-1);
		System.out.println("Fuel for modules: " + fuelForModules);
		
		final int totalFuel = moduleMasses.parallelStream().mapToInt(Integer::parseInt).map(Door1::getTotalFuelForModule).reduce((a, b) -> a + b).orElse(-1);
		System.out.println("Total fuel: " + totalFuel);
	}
	
	public static int getTotalFuelForModule(int moduleMass) {
		int fuelForModule = getFuelPerMass(moduleMass);
		
		int totalFuel = fuelForModule;
		int fuelForFuel = getFuelPerMass(fuelForModule);
		while (fuelForFuel > 0) {
			totalFuel += fuelForFuel;
			fuelForFuel = getFuelPerMass(fuelForFuel);
		}
		
		return totalFuel;
		
	}
	
	public static int getFuelPerMass(int mass) {
		return Math.max(mass / 3 - 2, 0);
	}
}
