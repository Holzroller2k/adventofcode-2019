package de.agonit.adventofcode.door1;

import static org.junit.Assert.*;

import org.junit.Test;

import de.agonit.adventofcode.door1.Door1;


public class Door1Test {

	@Test
	public void testGetFuelPerMass() {
		assertEquals(2, Door1.getFuelPerMass(12));
		assertEquals(2, Door1.getFuelPerMass(14));
		assertEquals(0, Door1.getFuelPerMass(8));
		assertEquals(0, Door1.getFuelPerMass(7));
		assertEquals(0, Door1.getFuelPerMass(6));
		assertEquals(0, Door1.getFuelPerMass(5));
		assertEquals(0, Door1.getFuelPerMass(4));
		assertEquals(0, Door1.getFuelPerMass(3));
	}
	
	@Test
	public void testGetTotalFuelForModule() throws Exception {
		assertEquals(966, Door1.getTotalFuelForModule(1969));
		assertEquals(50346, Door1.getTotalFuelForModule(100756));
	}

}
