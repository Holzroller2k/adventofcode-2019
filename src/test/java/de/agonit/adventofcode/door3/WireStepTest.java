package de.agonit.adventofcode.door3;

import static org.junit.Assert.*;

import org.junit.Test;


public class WireStepTest {

	@Test
	public void testFrom() {
		WireStep u40 = WireStep.from("U40");
		assertEquals(Direction.U, u40.direction);
		assertEquals(40, u40.distance);
	}

	@Test
	public void testGetVector() {
		WireStep u40 = WireStep.from("U40");
		WireStep d40 = WireStep.from("D40");
		WireStep l40 = WireStep.from("L40");
		WireStep r40 = WireStep.from("R40");
		
		Coordinate vectorU = u40.getVector();
		assertEquals(0, vectorU.x);
		assertEquals(40, vectorU.y);
		
		Coordinate vectorD = d40.getVector();
		assertEquals(0, vectorD.x);
		assertEquals(-40, vectorD.y);
		
		Coordinate vectorL = l40.getVector();
		assertEquals(-40, vectorL.x);
		assertEquals(0, vectorL.y);
		
		Coordinate vectorR = r40.getVector();
		assertEquals(40, vectorR.x);
		assertEquals(0, vectorR.y);
	}

}
