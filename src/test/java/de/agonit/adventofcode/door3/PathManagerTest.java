package de.agonit.adventofcode.door3;

import static org.junit.Assert.*;

import java.util.List;
import java.util.TreeSet;

import org.junit.Test;


public class PathManagerTest {

	@Test
	public void testGetCablePath() {
		List<WireStep> steps = Door3.getSteps("U4, R6, D5");
		List<CablePath> cablePath = new PathManager().getCablePath(steps);
		
		assertEquals(3, cablePath.size());
		
	}

}
