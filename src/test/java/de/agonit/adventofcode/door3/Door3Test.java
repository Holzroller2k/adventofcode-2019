package de.agonit.adventofcode.door3;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.junit.Test;


public class Door3Test {
	
	private static final String EXAMPLE_1_CABLE_1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72";
	private static final String EXAMPLE_1_CABLE_2 = "U62,R66,U55,R34,D71,R55,D58,R83";
	private static final String EXAMPLE_2_CABLE_1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51";
	private static final String EXAMPLE_2_CABLE_2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
	
	private static final String SIMPLE_CABLE_1 = "R8,U5,L5,D3";
	private static final String SIMPLE_CABLE_2 = "U7,R6,D4,L4";
	
	@Test
	public void testGetSteps() {
		List<WireStep> steps_1_1 = Door3.getSteps(EXAMPLE_1_CABLE_1);
		
		assertEquals(9, steps_1_1.size());
		assertEquals(new WireStep(Direction.U, 83), steps_1_1.get(3));
	}
	
	@Test
	public void testGetIntersections() {
		//Example 1
		List<WireStep> steps_1_1 = Door3.getSteps(EXAMPLE_1_CABLE_1);
		List<WireStep> steps_1_2 = Door3.getSteps(EXAMPLE_1_CABLE_2);
		
		List<CablePath> cablePath_1_1 = new PathManager().getCablePath(steps_1_1);
		List<CablePath> cablePath_1_2 = new PathManager().getCablePath(steps_1_2);
		
		Map<CablePath, List<Intersection>> intersections1 = Door3.getIntersections(cablePath_1_1, cablePath_1_2);
		
		List<Intersection> allIntersections = intersections1.values().parallelStream().flatMap(Collection::stream).collect(Collectors.toList());
		
		assertTrue(allIntersections.size() > 0);
		
		//Example 2
		List<WireStep> steps_2_1 = Door3.getSteps(EXAMPLE_2_CABLE_1);
		List<WireStep> steps_2_2 = Door3.getSteps(EXAMPLE_2_CABLE_2);
		
		List<CablePath> cablePath_2_1 = new PathManager().getCablePath(steps_2_1);
		List<CablePath> cablePath_2_2 = new PathManager().getCablePath(steps_2_2);
		
		Map<CablePath, List<Intersection>> intersections2 = Door3.getIntersections(cablePath_2_1, cablePath_2_2);
		
		List<Intersection> allIntersections2 = intersections2.values().parallelStream().flatMap(Collection::stream).collect(Collectors.toList());
		
		assertTrue(allIntersections2.size() > 0);
	}
	
	@Test
	public void testGetClosestIntersectionDistance() throws Exception {
		//Simple example
		List<WireStep> steps_0_1 = Door3.getSteps(SIMPLE_CABLE_1);
		List<WireStep> steps_0_2 = Door3.getSteps(SIMPLE_CABLE_2);
		
		List<CablePath> cablePath_0_1 = new PathManager().getCablePath(steps_0_1);
		List<CablePath> cablePath_0_2 = new PathManager().getCablePath(steps_0_2);
		
		Map<CablePath, List<Intersection>> intersections0 = Door3.getIntersections(cablePath_0_1, cablePath_0_2);
		
		Intersection closestIntersection0 = Door3.getClosestIntersection(intersections0).get();
		assertEquals(6, closestIntersection0.intersectionPoint.getDistanceToOutput());
		
		//Example 1
		List<WireStep> steps_1_1 = Door3.getSteps(EXAMPLE_1_CABLE_1);
		List<WireStep> steps_1_2 = Door3.getSteps(EXAMPLE_1_CABLE_2);
		
		List<CablePath> cablePath_1_1 = new PathManager().getCablePath(steps_1_1);
		List<CablePath> cablePath_1_2 = new PathManager().getCablePath(steps_1_2);
		
		Map<CablePath, List<Intersection>> intersections1 = Door3.getIntersections(cablePath_1_1, cablePath_1_2);
		
		Intersection closestIntersection1 = Door3.getClosestIntersection(intersections1).get();
		assertEquals(159, closestIntersection1.intersectionPoint.getDistanceToOutput());
		
		//Example 2
		List<WireStep> steps_2_1 = Door3.getSteps(EXAMPLE_2_CABLE_1);
		List<WireStep> steps_2_2 = Door3.getSteps(EXAMPLE_2_CABLE_2);
		
		List<CablePath> cablePath_2_1 = new PathManager().getCablePath(steps_2_1);
		List<CablePath> cablePath_2_2 = new PathManager().getCablePath(steps_2_2);
		
		Map<CablePath, List<Intersection>> intersections2 = Door3.getIntersections(cablePath_2_1, cablePath_2_2);
		
		Intersection closestIntersection2 = Door3.getClosestIntersection(intersections2).get();
		assertEquals(135, closestIntersection2.intersectionPoint.getDistanceToOutput());
	}

}
