package de.agonit.adventofcode.door3;

import static org.junit.Assert.*;

import org.junit.Test;


public class CoordinateTest {

	@Test
	public void testGetManhattenDistance() {
		Coordinate output = new Coordinate(0, 0);
		Coordinate intersection1 = new Coordinate(3, 3);
		Coordinate intersection2 = new Coordinate(-4, 3);
		Coordinate intersection3 = new Coordinate(4, -3);
		
		assertEquals(6, Coordinate.getManhattenDistance(output, intersection1));
		assertEquals(7, Coordinate.getManhattenDistance(output, intersection2));
		assertEquals(7, Coordinate.getManhattenDistance(output, intersection3));
	}
	
	@Test
	public void testGetDistanceToOutput() {
		assertEquals(6, new Coordinate(3,3).getDistanceToOutput());
		assertEquals(6, new Coordinate(-2,4).getDistanceToOutput());
		assertEquals(6, new Coordinate(4,-2).getDistanceToOutput());
	}

}
