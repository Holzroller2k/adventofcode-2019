package de.agonit.adventofcode.door3;

import static org.junit.Assert.*;

import org.junit.Test;

public class CablePathTest {

	private static final CablePath VERTICAL_UP_1 = new CablePath(new Coordinate(0, 0), new Coordinate(0, 4));
	private static final CablePath VERTICAL_UP_2 = new CablePath(new Coordinate(2, 2), new Coordinate(2, 4));

	private static final CablePath HORIZONTAL_RIGHT_1 = new CablePath(new Coordinate(0, 0), new Coordinate(4, 0));
	private static final CablePath HORIZONTAL_RIGHT_2 = new CablePath(new Coordinate(2, 2), new Coordinate(4, 2));

	private static final CablePath VERTICAL_DOWN_1 = new CablePath(new Coordinate(3, 5), new Coordinate(3, 2));
	private static final CablePath HORIZONTAL_RIGHT_3 = new CablePath(new Coordinate(2, 3), new Coordinate(6, 3));

	private static final CablePath HORIZONTAL_RIGHT_4_FAR = new CablePath(new Coordinate(10, 10), new Coordinate(14, 10));

	private static final CablePath VERTICAL_UP_PROOF = new CablePath(new Coordinate(0, 0), new Coordinate(75, 0));
	private static final CablePath HORIZONTAL_RIGHT_PROOF = new CablePath(new Coordinate(66, 62), new Coordinate(66, 117));

	@Test
	public void testGetIntersection() {
		// Test intersecting orthogonals
		assertEquals(new Coordinate(3, 3), VERTICAL_DOWN_1.getIntersection(HORIZONTAL_RIGHT_3).orElse(null).intersectionPoint);
		assertEquals(new Coordinate(3, 3), HORIZONTAL_RIGHT_3.getIntersection(VERTICAL_DOWN_1).orElse(null).intersectionPoint);

		// Test non intersecting orthogonals
		assertNull(VERTICAL_DOWN_1.getIntersection(HORIZONTAL_RIGHT_4_FAR).orElse(null));
		assertNull(HORIZONTAL_RIGHT_4_FAR.getIntersection(VERTICAL_DOWN_1).orElse(null));

		// Test non intersecting orthogonals proof
		assertNull(VERTICAL_UP_PROOF.getIntersection(HORIZONTAL_RIGHT_PROOF).orElse(null));
	}

	@Test
	public void testGetIntersectionWithParallels() {
		assertNull(VERTICAL_UP_1.getIntersection(VERTICAL_UP_2).orElse(null));
		assertNull(HORIZONTAL_RIGHT_1.getIntersection(HORIZONTAL_RIGHT_2).orElse(null));
	}

}
