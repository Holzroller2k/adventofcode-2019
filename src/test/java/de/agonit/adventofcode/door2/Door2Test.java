package de.agonit.adventofcode.door2;

import static org.junit.Assert.*;

import org.junit.Test;

import de.agonit.adventofcode.door2.Door2;

public class Door2Test {

	private static final int ITS_30 = 30;
	private static final int[] PROGRAM_OUTPUT_30 = new int[] { 1, 1, 1, 4, 99, 5, 6, 0, 99 };

	@Test
	public void testGetIntComputerOutputCode() {
		assertEquals(2, Door2.getIntComputerOutputCode((new int[] { 1, 0, 0, 0, 99 })));
		assertEquals(2, Door2.getIntComputerOutputCode((new int[] { 2, 3, 0, 3, 99 })));
		assertEquals(2, Door2.getIntComputerOutputCode((new int[] { 2, 4, 4, 5, 99, 0 })));
		assertEquals(ITS_30, Door2.getIntComputerOutputCode(PROGRAM_OUTPUT_30));
	}

	@Test
	public void testGetInputValuesForOutputCode() throws Exception {
		int[] intProgram = PROGRAM_OUTPUT_30;

		int[] inputValues2 = new int[] { 1, 0 };
		int[] inputValues3 = new int[] { 2, 2 };
		int[] inputValues4 = new int[] { 5, 6 };

		int[] testProgram1 = PROGRAM_OUTPUT_30;
		int[] testProgram2 = replaceWithInputValues(Door2.getCopy(intProgram), inputValues2);
		int[] testProgram3 = replaceWithInputValues(new int[] { 1, -1, -1, 5, 99, 5, 6, 0, 99 }, inputValues3);
		int[] testProgram4 = replaceWithInputValues(new int[] { 1, -1, -1, 6, 99, 5, 6, 0, 99 }, inputValues4);

		//Gather outputCodes for example intPrograms
		int outputCode1 = ITS_30;
		int outputCode2 = Door2.getIntComputerOutputCode(Door2.getCopy(testProgram2));
		int outputCode3 = Door2.getIntComputerOutputCode(Door2.getCopy(testProgram3));
		int outputCode4 = Door2.getIntComputerOutputCode(Door2.getCopy(testProgram4));

		//Calc inputValues for exampleOutputCodes
		int[] resultInputValues1 = Door2.getInputValuesForOutputCode(Door2.getCopy(intProgram), outputCode1);
		int[] resultInputValues2 = Door2.getInputValuesForOutputCode(Door2.getCopy(intProgram), outputCode2);
		int[] resultInputValues3 = Door2.getInputValuesForOutputCode(Door2.getCopy(intProgram), outputCode3);
		int[] resultInputValues4 = Door2.getInputValuesForOutputCode(Door2.getCopy(intProgram), outputCode4);

		int outputCodeForResultInputs1 = Door2.getIntComputerOutputCode(replaceWithInputValues(Door2.getCopy(testProgram1), resultInputValues1));
		assertEquals(outputCode1, outputCodeForResultInputs1);
		
		int outputCodeForResultInputs2 = Door2.getIntComputerOutputCode(replaceWithInputValues(Door2.getCopy(testProgram2), resultInputValues2));
		assertEquals(outputCode2, outputCodeForResultInputs2);
		
		int outputCodeForResultInputs3 = Door2.getIntComputerOutputCode(replaceWithInputValues(Door2.getCopy(testProgram3), resultInputValues3));
		assertEquals(outputCode3, outputCodeForResultInputs3);
		
		int outputCodeForResultInputs4 = Door2.getIntComputerOutputCode(replaceWithInputValues(Door2.getCopy(testProgram4), resultInputValues4));
		assertEquals(outputCode4, outputCodeForResultInputs4);
	}
	
	private static int[] replaceWithInputValues(int[] intProgramm, int[] inputValues) {
		intProgramm[1] = inputValues[0];
		intProgramm[2] = inputValues[1];
		
		return intProgramm;
	}
}
